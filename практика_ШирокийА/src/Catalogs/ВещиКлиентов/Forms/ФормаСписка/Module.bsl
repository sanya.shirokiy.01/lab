
&НаКлиенте
Процедура УстановитьОтбор()
	НайденыйОтбор = Неопределено;
	ПолеПометка = Новый ПолеКомпоновкиДанных("ПометкаУдаления");
	
	Для Каждого СуществующийОтбор Из Список.Отбор.Элементы Цикл
		Если СуществующийОтбор.ЛевоеЗначение = ПолеПометка Тогда
			НайденыйОтбор = СуществующийОтбор;
			Прервать;
		КонецЕсли;
	КонецЦикла;
	
	Если НайденыйОтбор = Неопределено Тогда
		НайденыйОтбор = Список.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
		НайденыйОтбор.ЛевоеЗначение = ПолеПометка;
		НайденыйОтбор.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
		НайденыйОтбор.ПравоеЗначение = Ложь;
		НайденыйОтбор.Использование = Истина;
	Иначе
		НайденыйОтбор.Использование = НЕ НайденыйОтбор.Использование;
	КонецЕсли;
	Элементы.ФормаПоказатьПомеченныеНаУдаление.Пометка = НЕ НайденыйОтбор.Использование;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	УстановитьОтбор();
КонецПроцедуры

&НаКлиенте
Процедура ПоказатьПомеченныеНаУдаление(Команда)
	УстановитьОтбор();
КонецПроцедуры
